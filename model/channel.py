# -*- coding: utf-8 -*-
import emoji
from peewee import *

import const
import helpers
from model.basemodel import BaseModel


class Channel(BaseModel):
    id = PrimaryKeyField()
    order = IntegerField(unique=True)
    emojis = CharField()
    name = CharField(unique=True)
    extra = CharField(null=True)
    current_message_id = IntegerField(null=True)

    @staticmethod
    def select_all():
        return Channel.select().order_by(Channel.order)

    

    def __str__(self):
        return '•' + emoji.emojize(self.emojis, use_aliases=True) + self.name + \
               (' - ' + self.extra if self.extra else '')
