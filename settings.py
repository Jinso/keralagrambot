import os

from decouple import config

if os.environ.get("DEV"):
    print('Debug/Development mode')
    DEV = True
else:
    DEV = False

### BOT CONFIGURATION ###
LOG_DIR = config('LOG_DIR', default=os.path.dirname(os.path.abspath(__file__)))
BOT_THUMBNAIL_DIR = config('BOT_THUMBNAIL_DIR',
                           default=os.path.expanduser(
                               './data/botlistbot/bot-profile-pictures'))
MODERATORS = [
    4623665,  # @afsal181
    7351948,  # @SpEcHiDe
    20516707,  # @jithumon
    47518346,  # @sajilck
    189487949,  # @aswinashok
    197549508,  # @ablets
    243330990,  # @mujeebcpy
    288203947,  # @muhammedunais
    293751493  # @asifak
]
ADMINS = [
    7351948,  # @SpEcHiDe
    243330990  # @mujeebcpy
]
BOT_CONSIDERED_NEW = 1  # Revision difference
WORKER_COUNT = 5 if DEV else 20
SELF_BOT_NAME = "spechidevahdamirhstestbot" if DEV else "botlistbot"
SELF_BOT_ID = "463726118" if DEV else "265482650"
BOTLIST_NOTIFICATIONS_ID = -313819499
BOTLISTCHAT_ID = -313819499 if DEV else -313819499
BLSF_ID = -313819499
SELF_CHANNEL_USERNAME = "spechidetestchannel" if DEV else "botlist"
REGEX_BOT_IN_TEXT = r'.*(@[a-zA-Z0-9_]{3,31}).*'
REGEX_BOT_ONLY = r'(@[a-zA-Z0-9_]{3,31})'
PAGE_SIZE_SUGGESTIONS_LIST = 5
PAGE_SIZE_BOT_APPROVAL = 5
MAX_SEARCH_RESULTS = 25
MAX_BOTS_PER_MESSAGE = 140
BOT_ACCEPTED_IDLE_TIME = 2  # minutes
SUGGESTION_LIMIT = 25
API_URL = "localhost" if DEV else "josxa.jumpingcrab.com"
API_PORT = 6060

USERBOT_SESSION = "./data/accounts/my2nd"
RUN_BOTCHECKER = False

DEBUG_LOG_FILE = "botlistbot.log"

if not os.path.exists(BOT_THUMBNAIL_DIR):
    os.makedirs(BOT_THUMBNAIL_DIR)
