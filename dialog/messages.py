import random

import captions
import mdformat

PROMOTION_MESSAGE = "*ᴊᴏɪɴ ᴛʜᴇ* @Keralagramlist 💙\n*sʜᴀʀᴇ* ʏᴏᴜʀ ʙᴏᴛs ɪɴ @Keralagrambot"
HELP_MESSAGE_ENGLISH = """ keralagramlist ചാനല്‍ പരിപാലിക്കുന്ന ബോട്ട് ആണ് ഞാന്‍.  നിങ്ങള്‍ക്ക് പുതിയ ഗ്രൂപ്പ്, ചാനല്‍, ബോട്ട് എന്നിവ ബോട്ടിലേക്ക് ചേര്‍ക്കാന്‍ നിര്‍ദേശിക്കുവാനും അഡ്മിന്‍സിന് അവ തെര‍ഞ്ഞെടുത്ത് അംഗീകരിക്കാനും ഉള്ള ഓട്ടോമേറ്റഡ് ബോട്ട് ആണ് ഇത്. ഒരു @keralagram സംരംഭം

▶️ *ആദ്യ പടികള്‍:*
1️⃣ /category എന്ന കമാന്റ് ഉപയോഗിച്ച് ബോട്ടിലെ ബട്ടണുകള്‍ ലഭ്യമാക്കി ആവശ്യമുള്ളത് ഞെക്കിയങ്ങോട്ട് തുടങ്ങാം.
2️⃣  inline സെര്‍ച്ച് വഴി  @keralagramlist വിഭാഗങ്ങള്‍ നിങ്ങളുടെ സുഹൃത്തുക്കള്‍ക്ക് അയക്കുന്നതിനായി (i.e. type `@KeralaGramBot music`) എന്നരീതിയില്‍ ഏത് ചാറ്റിലും ഉപയോഗിക്കാം.
3️⃣ keralagramlist അപ്ഡേറ്റുകള്‍ ലഭ്യമാക്കാനായി നിങ്ങളുടെ ഗ്രൂപ്പിലേക്ക് എന്നെ ചേര്‍ത്ത്  /subscribe ചെയ്തിടാവുന്നതാണ്. 
4️⃣ keralagrambot നകത്ത്  `#new @newbot🔎 - description` എന്ന രീതിയില്‍ പുതിയ യൂസര്‍നെയിമുകള്‍ നിര്‍ദേശിക്കാവുന്നതാണ്.

നിങ്ങള്‍ക്ക് ഏതെങ്കിലും യൂസര്‍നെയിം എനിക്ക് അയച്ചുതന്നാല്‍ നിലവില്‍ ബോട്ടില്‍ ലഭ്യമാണോ അല്ലയോ എന്ന് അറിയിക്കാം.

thank you for being with us 🤖"""
# HELP_MESSAGE_SPANISH = """*ɢʀᴇᴇᴛɪɴɢs ʜᴜᴍᴀɴᴏɪᴅs* 🤖
#
# Soy el bot encargado de mantener el canal @BotList y proporcionar a los usuarios de Telegram como tú el *catálogo de bot más fiable e imparcial* de una _manera interactiva_.
#
# ▫️ Agregame a tus grupos y recibe una notificación cuando se actualice el @BotList.
# ▫️ Envíeme acategorías individuales del @BotList a tus amigos a través de búsqueda en línea (p.e: escribe @botlistbot música en cualquier chat).
# ▫️ Únete a la comunidad @BotListChat y contribuye al BotList: #new @ nuevobot🔎 - descripción
#
# Primeros pasos: Empieza con el comando /category y utiliza los botones
# disponibles en pantalla desde ahí.
#
# Un paso más cerca de la dominación mundial... 🤖
# """
CONTRIBUTING = """You can use the following `#tags` with a Telegram `@username` to contribute to the keralagramlist:

• #new — Submit a fresh group/channel/bot. Use 🔎 if it supports inline queries and flag emojis to denote the language. Everything after the `-` character can be your description of the bot.
• #offline — Mark a bot as offline.
• #spam — Tell us that a bot spams too much.

There are also the corresponding /new, /offline and /spam commands.
The moderators will approve your submission as soon as possible.

*Next step*: Have a look at the /examples!
"""
EXAMPLES = """*Examples for contributing to the @KeralaGramList:*

• "Wow! I found this nice #new item: @coolbot 🔎🇮🇹 - Cools your drinks in the fridge."
• /new @coolbot 🔎🇮🇹 - Cools your drinks in the fridge.

• "Oh no... guys?! @unresponsive\_bot is #offline 😞"
• /offline @unresponsive\_bot

• "Aaaargh, @spambot's #spam is too crazy!"
• /spam @spambot
"""
REJECTION_WITH_REASON = """Sorry, but your bot submission {} was rejected.

Reason: {reason}

Please adhere to the quality standards we impose for inclusion to the @BotList.
For further information, please ask in the @BotListChat."""
REJECTION_PRIVATE_MESSAGE = """Sorry, but your bot submission {} was rejected.

It does not suffice the standards we impose for inclusion in the @BotList for one of the following reasons:

▫️A better bot with the same functionality is already in the @BotList.
▫️The user interface is bad in terms of usability and/or simplicity.
▫The bot is still in an early development stage
▫️Contains ads or exclusively adult content
▫️English language not supported per default (exceptions are possible)
▫NO MANYBOTS!!! 👺

For further information, please ask in the @BotListChat."""
ACCEPTANCE_PRIVATE_MESSAGE = """Congratulations, your bot submission {} has been accepted for the @BotList. You can already see it by using the /category command, and it is going to be in the @BotList in the next two weeks.\n\nCategory: {}"""
BOTLIST_UPDATE_NOTIFICATION = """⚠️@BotList *update!*
There are {n_bots} new bots:

{new_bots}

Share your bots in @BotListChat"""
SEARCH_MESSAGE = mdformat.action_hint("What would you like to search for?")
SEARCH_RESULTS = """I found *{num_results} bot{plural}* in the @BotList for *{query}*:\n
{bots}
"""
KEYWORD_BEST_PRACTICES = """The following rules for keywords apply:
▫️Keep the keywords as short as possible
▫️Use singular where applicable (#̶v̶i̶d̶e̶o̶s̶ video)
▫️Try to tag every supported platform (e.g. #vimeo, #youtube, #twitch, ...)
▫Try to tag every supported action (#search, #upload, #download, ...)
▫Try to tag every supported format (#mp3, #webm, #mp4, ...)
▫Keep it specific (only tag #share if the bot has a dedicated 'Share' button)
▫Tag bots made with _bot creators_ (e.g. #manybot)
▫Use #related if the bot is not standalone, but needs another application to work properly, e.g. an Android App
▫Always think in the perspective of a user in need of a bot. What query might he be putting in the search field?
"""
NEW_BOTS_INLINEQUERY = "New Items"
SELECT_CATEGORY = "Please select a category"
SHOW_IN_CATEGORY = "Show category"
REROUTE_PRIVATE_CHAT = mdformat.action_hint("Please use this command in a private chat or make use of inlinequeries.")
BOTLISTCHAT_RULES = """*Here are the rules for @keralagram:*\n\n

[Rules:](http://telegra.ph/KeralaGram-Rules-2017-05-02)"""
BAN_MESSAGE = mdformat.action_hint("Please send me the username to ban and remove all bot submissions")
UNBAN_MESSAGE = mdformat.action_hint("Please send me the username of the user to revoke ban state for")
FAVORITES_HEADLINE = "*{}* 🔽\n_┌ from_ @BotList".format(captions.FAVORITES)
ADD_FAVORITE = mdformat.action_hint("Please send me the @username of a bot to add to your favorites")

BOTPROPERTY_STARTSWITH = "Please send me a "
SET_BOTPROPERTY = "Please send me a {} for {} ― `{}` to clear"



def rand_call_to_action():
    choices = ["Check out", "You might like", "What about", "You should try", "Have a look at", "Why don't you try"]
    return random.choice(choices)


def rand_thank_you_slang():
    choices = ["👍🏼 Wow great, thank you!", "👍🏼 Good Job!", "❤️ Nice! Thanks", "❤️ Cool!", "❤️ Awesome!", "😍 Excellent!",
               "👌 You da man!", "👌 That's just perfect!", "👌 Well done!", "🙏 Good one!", "🙏 Great, keep it up!",
               "👏 I like it!", "👏 Orng Nice!"]
    return random.choice(choices)
